from statistics import mode
from django.db import models

# Create your models here.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200,unique=True)
    name = models.CharField(max_length=200)
    
    def __str__(self) -> str:
        return f"import_href is: {self.import_href}"

class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=100)
    pic_url = models.URLField()
    
    location = models.ForeignKey(LocationVO,related_name='hats',on_delete=models.CASCADE)

    def __str__(self):
        return f"A {self.color}, {self.style_name}, {self.fabric} Hat"
    
    