# Wardrobify

Team:

* Gary T. - Hats Microservice
* Jason O. - Which Shoes Microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice


models:
    Hat: hold hat's detail info
    LocationVO: poll data from wardrobe every 60s

apis:
    group api: 
        /hats/ or /locationID/hats -> api_list_hats handle GET list of hat and POST new hat and list all hats for a location
    single api:
        /hats/hatPK -> api_detail_hats handle GET hat detail, PUT update hat and DELETE a hat.

