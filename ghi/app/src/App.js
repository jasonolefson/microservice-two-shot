import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import Shoes from './shoes/shoes'
import ShoeForm from './shoes/ShoeForm';
import UpdateShoe from './shoes/UpdateShoe'

import Hats from './hats/Hats';
import HatForm from './hats/HatForm';

import UpdateHat from './hats/UpdateHat';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container w-75">
        <Routes>

          <Route path="/" element={<MainPage />}></Route>

          <Route path="hats" element={<Hats />} ></Route>
          <Route path="hats/add" element={<HatForm />}></Route>
          <Route path='hats/:id' element={<UpdateHat />} />
          {/* <Route path="/shoes" element={<Shoes />} /> */}




          <Route path="shoes" element={<Shoes />} ></Route>
          <Route path="shoes/add" element={<ShoeForm />} ></Route>
          <Route path="shoes/:id" element={<UpdateShoe />} ></Route>



          {/* <Route path="/shoes" element={<Shoes />} /> */}

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;


