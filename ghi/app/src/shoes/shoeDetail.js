import React, { Component } from "react";

export default class shoeDetail extends Component {
    constructor(props) {
        super(props);
        this.state={


        };
    }
    async componentDidMount(){
        const resp = await fetch ("http://localhost:8080/api/shoes/1/");
        const data = await resp.json();

        this.setState({...data})
    }



    render() {
        return (
        <div className="pb-3 bg-secondary">
            <h3>{this.state.manufacturer}</h3>
            <h3>{this.state.model_name}</h3>
            <h3>{this.state.color}</h3>
            <img src={this.state.pic_url} alt=""/>
            <hr/>
            <button className="btn btn-info">Delete</button>
            <button className="btn btn-dark">Update</button>
        </div>
        )
    }
}