import AccordionItem from './AccordionItem';
import { useState, useEffect } from 'react';
import React, { Component } from "react";



// export default function Accordion() {
//     const [shoes, setShoes] = useState([]);
//     const [refresh, setRefresh] = useState(1);

//     useEffect(() => {
//         console.log("refreshed!");
//         fetch("http://localhost:8080/api/shoes/")
//             .then(res => res.json())
//             .then(res => res)
//             .then(data => setShoes(data.shoes))
//     }, [refresh])

//     return (
//         <div id="accordion">
//             {shoes.map((shoe) => <AccordionItem key={shoe.id} data={shoe} refresh={refresh} setRefresh={setRefresh} />)}
//         </div>
//     )
// }

export default class Accordion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shoes: [],
            refresh: 1
        }
    }
    refresh = () => {
        this.setState({
            refresh: this.state.refresh + 1
        })
        console.log('refreshed!', this.state.refresh);
    }

    async componentDidMount() {
        const request = await fetch("http://localhost:8080/api/shoes/");
        const shoeData = await request.json();
        this.setState({
            shoes: shoeData.shoes,
        })
    }

    render() {
        return (
            <div id="accordion">
                {this.state.shoes.map(shoe => <AccordionItem key={shoe.id} data={shoe} refresh={this.refresh} />)}
            </div>
        )
    }
}
