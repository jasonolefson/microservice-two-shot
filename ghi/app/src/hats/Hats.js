import React, { Component } from 'react'
import Accordion from './Accordion';
import { Link } from 'react-router-dom';

export default class Hats extends Component {

    render() {
        return (
            <div className='grid text-center mt-5'>
                <div className="container row">
                    <h1 className='col-10' >Here are hats:</h1>
                    <Link to="/hats/add" className='col-2 btn btn-success'>Add Hat</Link>
                </div>
                <Accordion />
            </div>
        )
    }
}
