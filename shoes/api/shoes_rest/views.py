from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import BinVO, Shoe
from .encoders import ShoeDetailEncoder, ShoeListEncoder
# Create your views here.



#handle List: GET, POST
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_id=None):
    #handle get
    # if request.method == "GET":
    #     if bin_id == None:
    #         shoes = Shoe.objects.all()
    if request.method == "GET":
        if bin_id == None:
            shoes = {"shoes":Shoe.objects.all()}
        else:
            shoe_list = Shoe.objects.filter(bin=bin_id)
            shoes = {"shoes":shoe_list}
        return JsonResponse(shoes, encoder=ShoeListEncoder, safe=False)

    # #handle post
    else:
        content = json.loads(request.body)

        try:
            bin_href = content['bin']
            bin = BinVO.objects.get(import_href=bin_href)
            #assign BinVO to content ["bin"]
            content["bin"] = bin
        except:
            return JsonResponse({"message": "this BinVO does not exist in DB"})

        
        #Create new shoe in DB
        new_shoe = Shoe.objects.create(**content)
        #return new hat detail as json for the display
        return JsonResponse(new_shoe, encoder=ShoeListEncoder, safe=False)
        

#handle Detail: GET, PUT, DELETE
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoe(request,pk):
    #GET show hat detail
    if request.method ==  "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)

        #delete
    elif request.method == "DELETE":
        count,_ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count >0})

    else:
        #1 turn req json into python obj:
        content = json.loads(request.body)

    #2 checking binVO

        try:
            bin_href = content['bin']
            bin = BinVO.objects.get(import_href=bin_href)
            #assign BinVO to content[bin]
            content["bin"] = bin
        except:
            return JsonResponse({"message": "no this BinVO in DB"})

        #3 Update Shoe
        Shoe.objects.filter(id=pk).update(**content)

        #4 get the shoe with new info
        new_shoe = Shoe.objects.get(id=pk)
        return JsonResponse(new_shoe, encoder=ShoeDetailEncoder, safe=False)
